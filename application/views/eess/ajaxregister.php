<?php 
$attributes=array('class'=>'form-horizontal','id'=>'form_create','autocomplete'=>'off');
echo form_open_multipart('eess/register_ok',$attributes);
?>
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title"><i class="fa fa-pencil-square-o"></i> <?php echo ucwords('editar evento significativo');?></h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-xs-12">
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="input_name"><?php echo ucwords('correlativo');?></label>
					<div class="col-md-9 col-sm-12 col-xs-12">
						<input type="text" class="form-control" id="correlativo" placeholder="######" name="form[CORRELATIVO]" value="<?php if (isset($es['CORRELATIVO'])) echo $es['CORRELATIVO']; else echo "";?>" required readonly >
					</div>
				  </div>			  
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>
		<button id="btn-ok" type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Aceptar</button>
	</div>
</form>