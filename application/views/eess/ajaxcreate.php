<?php 
$attributes=array('class'=>'form-horizontal','id'=>'form_create','autocomplete'=>'off');
echo form_open_multipart('eess/create_ok',$attributes);
?>
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title"><i class="fa fa-pencil-square-o"></i> <?php echo ucwords('crear evento significativo');?></h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-xs-12">
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-12 col-xs-12" for="input_name"><?php echo ucwords('tipo');?></label>
					<div class="col-md-9 col-sm-12 col-xs-12">
						<select class="form-control" name="form[codigo]"  id="filter_Proveedor" required >
							<option value="">Seleccione...</option>
							<?php
							if(isset($pes)){
								foreach($pes as $p){
									?>
									<option value="<?php echo $p['CODIGO'];?>"><?php echo $p['CODIGO'].' '.$p['DESCRIPCION'];?></option>
									<?php
								}
							}
							?>
						</select>
						<p id="filter_Proveedor_error" style="color:red;"></p>
					</div>
				  </div>
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="input_name"><?php echo ucwords('descripcion');?></label>
					<div class="col-md-9 col-sm-12 col-xs-12">
<textarea style="width:100%;resize:none;" name="form[descripcion]" required >
</textarea>
						<p id="filter_Proveedor_error" style="color:red;"></p>
					</div>
				  </div>
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="input_name"><?php echo ucwords('Fechahora Ini');?></label>
					<div class="col-md-3 col-sm-12 col-xs-12">
						<input type="text" class="form-control" id="fechahora_ini" placeholder="yyyy-mm-dd HH:mm:ss" value="" name="form[fechahoraini]" required >
					</div>
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="input_name"><?php echo ucwords('Fechahora Fin');?></label>
					<div class="col-md-3 col-sm-12 col-xs-12">
						<input type="text" class="form-control" id="fechahora_fin" placeholder="yyyy-mm-dd HH:mm:ss" value="" name="form[fechahorafin]" required >
					</div>
				  </div>				  
				  <div class="form-group myrow">
					&nbsp;
				  </div>				  
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>
		<button id="btn-ok" type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Aceptar</button>
	</div>
</form>


<script type="text/javascript">

$(document).ready(function() {

	$('#fechahora_ini').datetimepicker({
	   format : 'YYYY-MM-DD HH:mm:ss'
	});
	$('#fechahora_fin').datetimepicker({
	   format : 'YYYY-MM-DD HH:mm:ss'
	});	
	
});

</script>