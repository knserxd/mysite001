

<div class="right_col" role="main">
	<div class="col-md-12 col-sm-12 col-xs-12">
	
		<div class="x_panel">
			<div class="x_title">
				<h2><?php echo ucwords(strtolower('eventos significativos'));?><small><?php echo ucwords(strtolower('administracion'));?></small></h2>
				<div class="clearfix"></div>
			</div>
		</div>	
		  
		<div class="x_panel">
			<div class="x_content">
				<p class="pull-left">
					<a href="javascript:void(0);" class="btn btn-primary" onclick="callCreate(this);">
						<i class="fa fa-plus"></i>
					</a>
					<a href="javascript:void(0);" class="btn btn-primary">
						<i class="fa fa-search"></i>
					</a>					
				</p>
				<div class="x_content">
					<table id="tableTramites" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th style="text-align:center"><?php echo ucwords(strtolower('eventos significativos'));?></th>
							</tr>
						</thead>
						<tbody>
							<?php
								if(isset($eesshists)){
									foreach($eesshists as $eesshist){
										
										$style=null;
										$style['estado']['label']="width:100%;background:#F4A82F;color:white;padding:3px;text-align:center;";
										$style['estado']['input']="width:100%;background:lightgray;";
										$eesshist['ESTADO_TEXT']="INACTIVO";
										if(isset($eesshist['ESTADO'])){
											if(strcmp($eesshist['ESTADO'],"A")==0){
												$style['estado']['label']="width:100%;background:#00B291;color:white;padding:3px;text-align:center;";
												$style['estado']['input']="width:100%;";
												$eesshist['ESTADO_TEXT']="ACTIVO";
											}
										}
							?>
							<tr>
								<td>
									<div class="col-md-1 col-sm-12 col-xs-12">
										<div class="row">
											<div class="col-md-12 col-sm-4 col-xs-4" style="text-align:center;">
												<b>
													<?php echo $eesshist['TIPO'];?>
												</b>
											</div>
											<div class="col-md-12 col-sm-4 col-xs-4" style="text-align:center;">
												<a href="javascript:void(0);" onclick="callEdit(this);" class="btn btn-xs btn-warning" id="<?php echo $eesshist['CORRELATIVO']?>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Editar"><i class="fa fa-pencil"></i></a>
											</div>
											<div class="col-md-12 col-sm-4 col-xs-4" style="text-align:center;">
												<a href="javascript:void(0);" onclick="callRegister(this);" class="btn btn-xs btn-primary" id="<?php echo $eesshist['CORRELATIVO']?>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Registrar"><i class="fa fa-paper-plane"></i></a>
											</div>											
										</div>
									</div>								
									<div class="col-md-11 col-sm-12 col-xs-12">
										<div class="row">
											<div class="col-md-6 col-sm-12 col-xs-12">
												<div class="row">
													<div class="col-md-4 col-sm-6 col-xs-6">
														<b><?php echo ucwords(strtolower('id'));?> :</b>
													</div>
													<div class="col-md-8 col-sm-6 col-xs-6">
														<?php echo $eesshist['CORRELATIVO'];?>	
													</div>
												</div>											
												<div class="row">
													<div class="col-md-4 col-sm-6 col-xs-6">
														<b><?php echo ucwords(strtolower('codigo'));?> :</b>
													</div>
													<div class="col-md-8 col-sm-6 col-xs-6">
														<?php echo $eesshist['CODIGO'];?>
													</div>
												</div>
												<div class="row">
													<div class="col-md-4 col-sm-6 col-xs-6">
														<b><?php echo ucwords(strtolower('descripcion'));?> :</b>
													</div>
													<div class="col-md-8 col-sm-6 col-xs-6">
														<?php echo $eesshist['DESCRIPCION'];?>
													</div>
												</div>
												<div class="row">
													<div class="col-md-4 col-sm-6 col-xs-6">
														<b><?php echo ucwords(strtolower('codigo recepcion'));?> :</b>
													</div>
													<div class="col-md-8 col-sm-6 col-xs-6">
														<?php echo $eesshist['CODIGO_RECEPCION'];?>	
													</div>
												</div>
											</div>
											<div class="col-md-6 col-sm-12 col-xs-12">
												<div class="row">
													<div class="col-md-4 col-sm-6 col-xs-6">
														<b><?php echo ucwords(strtolower('fecha ini'));?> :</b>
													</div>
													<div class="col-md-8 col-sm-6 col-xs-6">
														<?php echo $eesshist['FECHAINI'];?>	
													</div>
												</div>
												<div class="row">
													<div class="col-md-4 col-sm-6 col-xs-6">
														<b><?php echo ucwords(strtolower('fecha fin'));?> :</b>
													</div>
													<div class="col-md-8 col-sm-6 col-xs-6">
														<?php echo $eesshist['FECHAFIN'];?>	
													</div>
												</div>										

												<div class="row">
													<div class="col-md-4 col-sm-6 col-xs-6">
														<b><?php echo ucwords(strtolower('estado'));?> :</b>
													</div>
													<div class="col-md-8 col-sm-6 col-xs-6">
														<span><b style="<?php echo $style['estado']['label'];?>"><?php echo $eesshist['ESTADO_TEXT'];?></b></span>
													</div>
												</div>
											</div>										
										</div>
										<div class="row">
											<div class="col-md-2 col-sm-12 col-xs-12">
												<b><?php echo ucwords(strtolower('respuesta'));?> :</b>
											</div>
											<div class="col-md-10 col-sm-12 col-xs-12">
												<textarea style="width:100%;height:100px;resize:none;" readonly>
<?php 
echo (json_encode(json_decode($eesshist['RESPUESTA']),JSON_PRETTY_PRINT));
?>	
												</textarea>
											</div>											
										</div>										

									</div>
								</td>
							</tr>
							<?php
									}
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>				
				


<div class="modal fade bs-example-modal-lg" id="modal-dialog-create" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa fa-gears"></i> <?php echo ucwords('Procesando');?></h4>
			</div>
			<div class="modal-body">
				<center>
					<img src="<?php echo base_url()?>resources/images/ajax-loader.gif">
				</center>
			</div>
		</div>
	</div>
</div>

<div class="modal fade bs-example-modal-lg" id="modal-dialog-edit" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa fa-gears"></i> <?php echo ucwords('Procesando');?></h4>
			</div>
			<div class="modal-body">
				<center>
					<img src="<?php echo base_url()?>resources/images/ajax-loader.gif">
				</center>
			</div>
		</div>
	</div>
</div>

<div class="modal fade bs-example-modal-lg" id="modal-dialog-register" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa fa-gears"></i> <?php echo ucwords('Procesando');?></h4>
			</div>
			<div class="modal-body">
				<center>
					<img src="<?php echo base_url()?>resources/images/ajax-loader.gif">
				</center>
			</div>
		</div>
	</div>
</div>
		
<script src="<?php echo base_url()?>resources/js/jquery.min.js"></script>
<script src="<?php echo base_url()?>resources/plugins/daterangepicker/moment.min.js" type="text/javascript"></script>

<script type="text/javascript">

var oTableBrigadas;

$(document).ready(function() {
	

	oTableTramites = $('#tableTramites').dataTable({
		//"pageLength": 50,
		//"bPaginate": false,
		//"info": false,
		"deferLoading": 2,
		"aaSorting": [],
		"bSort": false,
	});
	
	/*
	$(".widget-process").click(function(e){
		e.preventDefault();
		ajaxprocess();
		//$('#modal-dialog-create').modal('show');
	});	
	*/
	
	//reloadTable();
	//validateFilter();
});

function callCreate(obj){
	ajaxcreate();
	$('#modal-dialog-create').modal('show');
}

	function ajaxcreate() {

		$.ajax({
			 type: "POST",
			 url: "<?php echo base_url()?>eess/ajaxcreate/",
			 /*data: {id},*/
			 dataType: "text",  
			 cache:false,
			 success: 
				  function(data){
					$('#modal-dialog-create .modal-content').html(data);
				  }
			  });
	}

function callEdit(obj){
	var id=$(obj).attr('id');
	ajaxedit(id);
	$('#modal-dialog-edit').modal('show');
}

	function ajaxedit(correlativo) {

		$.ajax({
			 type: "POST",
			 url: "<?php echo base_url()?>eess/ajaxedit/",
			 data: {"correlativo":correlativo},
			 dataType: "text",  
			 cache:false,
			 success: 
				  function(data){
					$('#modal-dialog-edit .modal-content').html(data);
				  }
			  });
	}	
	
function callRegister(obj){
	var id=$(obj).attr('id');
	ajaxregister(id);
	$('#modal-dialog-register').modal('show');
}

	function ajaxregister(correlativo) {

		$.ajax({
			 type: "POST",
			 url: "<?php echo base_url()?>eess/ajaxregister/",
			 data: {"correlativo":correlativo},
			 dataType: "text",  
			 cache:false,
			 success: 
				  function(data){
					$('#modal-dialog-register .modal-content').html(data);
				  }
			  });
	}
	

		
</script>
<style>
.table_be{
	border-collapse:collapse;
}
.table_be th{
	text-align:center;
	padding:3px;
}
.table_be td{
	padding:3px;
}

b{
	color:#286090;
}

</style>
