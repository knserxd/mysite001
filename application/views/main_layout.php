<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>S.I.A.T - ELFEC</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url()?>resources/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url()?>resources/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url()?>resources/css/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo base_url()?>resources/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <!--<link href="<?php echo base_url()?>resources/plugins/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">-->
	
    <!-- Datatables -->
    <link href="<?php echo base_url()?>resources/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>resources/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>resources/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>resources/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>resources/css/scroller.bootstrap.min.css" rel="stylesheet">

	<!-- datetimepicker -->
	<link href="<?php echo base_url()?>resources/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">		
	
    <!-- Custom Theme Style -->
    <link href="<?php echo base_url()?>resources/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
              <a href="index.html" class="site_title"><img src="<?php echo base_url()?>resources/images/logo_transp.png" style="width:25px;"> <span>S.I.A.T - ELFEC</span></a>
            <div class="navbar nav_title" style="border: 0;">
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url()?>resources/images/logo_raaf.png" alt="..." class="img-circle profile_img">
              </div>
			<?php 
				if (isset($menuBienvenido)){
					?>
              <div class="profile_info" style="color:white;">
                Bienvenido:
					</br>
				<?php 
					echo $menuBienvenido['username'];
				?>
              </div>
					<?php
				}else{
					
				}
			?>

			  
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
					<?php if (isset($menuLateral)) echo $menuLateral; else echo "";?>
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->
			
            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings" href="#">
                &nbsp;
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen" href="#">
                &nbsp;
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock" href="#">
                &nbsp;
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo base_url()?>logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true" style="color:red;"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
					<img src="<?php echo base_url()?>resources/images/logo_raaf.png" alt="">
					<?php 
						if (isset($menuBienvenido)){
							echo $menuBienvenido['username'];
						}else{
						}
					?>				
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="<?php echo base_url()?>logout"><i class="fa fa-sign-out pull-right"></i><b style="color:red;"> Cerrar Session</b></a></li>
                  </ul>
                </li>
				
				
                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-users"></i>
					BRIGADAS
					<span class=" fa fa-angle-down"></span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
					<?php
					if(isset($menuBienvenido['empleadobrigadas'])){
						foreach($menuBienvenido['empleadobrigadas'] as $empleadobrigada){
							?>
							<li>
								<span><?php echo $empleadobrigada['BRIGADA']['DESCRIPCION'];?></span>
							</li>								
							<?php
						}
					}
					?>				  
                  </ul>
                </li>

                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-key"></i>
					ROLES
					<span class=" fa fa-angle-down"></span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
					<?php
					if(isset($menuBienvenido['empleadoroles'])){
						foreach($menuBienvenido['empleadoroles'] as $empleadorol){
							?>	
							<li>
								<span><?php echo $empleadorol;?></span>
							</li>							
							<?php
						}
					}
					?>
                  </ul>
                </li>
				
		
				
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
			<?php 
			if(isset($content_for_layout)){
				echo $content_for_layout;
			}
			?>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
	
	    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url()?>resources/js/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url()?>resources/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url()?>resources/js/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url()?>resources/js/nprogress.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url()?>resources/js/icheck.min.js"></script>
	
    <!-- bootstrap-progressbar -->
    <!--<script src="<?php echo base_url()?>resources/plugins/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>	-->
    <!-- Datatables -->
    <script src="<?php echo base_url()?>resources/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url()?>resources/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>resources/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url()?>resources/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>resources/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url()?>resources/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url()?>resources/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url()?>resources/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url()?>resources/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url()?>resources/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url()?>resources/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url()?>resources/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url()?>resources/js/jszip.min.js"></script>
    <script src="<?php echo base_url()?>resources/js/pdfmake.min.js"></script>
    <script src="<?php echo base_url()?>resources/js/vfs_fonts.js"></script>
	
	<!-- moment -->
	<script src="<?php echo base_url()?>resources/js/moment.min.js"></script>
	<!-- datetimepicker -->	
	<script src="<?php echo base_url()?>resources/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>		

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url()?>resources/js/custom.min.js"></script>
	
	<!-- Validate -->
	<script src="<?php echo base_url()?>resources/js/jquery.validate.js"></script>	
			
  </body>
</html>
<style>
   .error {
      color: red;
   }
</style>